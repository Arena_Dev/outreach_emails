var gulp = require('gulp'),
  inlineCss = require('gulp-inline-css');

gulp.task('default', function () {
  return gulp
    .src('./pages/**/*.html')
    .pipe(
      inlineCss({
        applyStyleTags: false,
        applyLinkTags: true,
        removeStyleTags: true,
        removeLinkTags: true,
      }),
    )
    .pipe(gulp.dest('build/'));
});
